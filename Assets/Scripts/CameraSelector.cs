﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace ReplayNow {


    public class CameraSelector : MonoBehaviour {
        [SerializeField] private CameraDisplay cameraDisplay;
        private Dropdown dropdown;

        private void Start() {
            dropdown = GetComponent<Dropdown>();
            
            dropdown.ClearOptions();
            var webCamDevices = WebCamTexture.devices;
            dropdown.AddOptions(webCamDevices.Select(device => device.name).ToList());
            
            cameraDisplay.CreateWebCamTexture(webCamDevices[0]);
        }

        public void SetWebCamDevice(int i) {
            cameraDisplay.CreateWebCamTexture(WebCamTexture.devices[i]);
        }
    }
}
