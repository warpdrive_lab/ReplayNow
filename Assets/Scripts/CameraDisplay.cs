﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ReplayNow {
    public class CameraDisplay : MonoBehaviour {

        private RawImage display;
        private AspectRatioFitter aspectFitter;
        private WebCamTexture texture;
        private Queue<RenderTexture> queue = new Queue<RenderTexture>();
        private RenderTexture last;
        private int delay = 10;

        [SerializeField] private GameObject loadingPanel;
        [SerializeField] private Text loadingText;
        [SerializeField] private Image loadingProgress1;
        [SerializeField] private Image loadingProgress2;
        [SerializeField] private Toggle mirrorToggle;
        
        private IEnumerator Start() {
            display = GetComponent<RawImage>();
            aspectFitter = GetComponent<AspectRatioFitter>();
            if( WebCamTexture.devices.Length == 0 )
            {
                Debug.LogFormat( "カメラのデバイスが無い様だ。撮影は諦めよう。" );
                yield break;
            }

            yield return Application.RequestUserAuthorization( UserAuthorization.WebCam );
            if( !Application.HasUserAuthorization( UserAuthorization.WebCam ) )
            {
                Debug.LogFormat( "カメラを使うことが許可されていないようだ。市役所に届けでてくれ！" );
                yield break;
            }
        }

        public void CreateWebCamTexture(WebCamDevice device) {
            if (texture) texture.Stop();
            ClearTextureQueue();
            if (string.IsNullOrEmpty(device.name)) {
                texture = null;
                return;
            }

            texture = new WebCamTexture(device.name, 1920, 1080, 60);
            texture.Play();
        }

        private void Update() {
            if (texture == null) return;

            aspectFitter.aspectRatio = ((float) texture.width) / ((float) texture.height);
            transform.localScale = new Vector3(mirrorToggle.isOn ? -1 : 1, 1, 1);
            var render = RenderTexture.GetTemporary(texture.width, texture.height);
            Graphics.Blit(texture,render);
            
            queue.Enqueue(render);
            if (queue.Count > delay*60) {
                loadingPanel.SetActive(false);
                if (last) RenderTexture.ReleaseTemporary(last);
                last = queue.Dequeue();
                display.texture = last;
                while (queue.Count > delay*60) {
                    RenderTexture.ReleaseTemporary(queue.Dequeue());
                }
            } else {
                loadingPanel.SetActive(true);
                var frac = queue.Count / ((float) delay * 60);
                loadingText.text = $"{(int) (frac * 100)}%";
                loadingProgress1.fillAmount = frac;
                loadingProgress2.fillAmount = frac;
            }
        }

        private void OnDestroy() {
            texture.Stop();
            ClearTextureQueue();
        }

        public void ChangeDelay(float value) {
            var intValue = (int) value;
            if (delay < intValue) {
                display.texture = null;
            } else {
                var len = queue.Count;
                while (len-- > 60 * intValue) {
                    RenderTexture.ReleaseTemporary(queue.Dequeue());
                }
            }
            delay = intValue;
        }

        private void ClearTextureQueue() {
            for (var i = 0; i < queue.Count; i++) {
                RenderTexture.ReleaseTemporary(queue.Dequeue());
            }
        }
    }
}