﻿using UnityEngine;
using UnityEngine.UI;

public class DelaySliderValueIndicator : MonoBehaviour {
    private Text text;

    private void Start() {
        text = GetComponent<Text>();
    }

    public void OnValueChanged(float value) {
        text.text = $"{(int) value} 秒";
    }
}
