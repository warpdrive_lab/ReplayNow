using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ReplayNow {
    public class ControlPanelMove : MonoBehaviour {
        private bool show = true;
        private RectTransform rectTr;
        [SerializeField] private Text buttonLabel;
        [SerializeField] private float autoCloseTime;

        private void Start() {
            rectTr = (RectTransform) transform;
            Invoke(nameof(Close), autoCloseTime);
        }

        public void OnPointerEnter(BaseEventData e) {
            CancelInvoke(nameof(Close));
        }

        public void OnPointerExit(BaseEventData e) {
            autoCloseTime = 10f;
            CancelInvoke(nameof(Close));
            Invoke(nameof(Close), autoCloseTime);
        }

        private void Close() {
            if (show) {
                rectTr.DOPivotX(1f, .5f);
                show = false;
                buttonLabel.text = ">>";
            }
        }

        public void OnClick() {
            CancelInvoke(nameof(Close));
            if (show) {
                rectTr.DOPivotX(1f, .5f);
            } else {
                rectTr.DOPivotX(0f, .5f);
            }

            show = !show;
            buttonLabel.text = show ? "<<" : ">>";
        }
    }
}